package museum;

import museum.controller.Inventorisation;
import museum.controller.MuseumWriter;
import museum.dao.impl.ExhibitDaoImpl;
import museum.dao.impl.InventoryDaoImpl;
import museum.model.Exhibit;

import java.sql.SQLException;
import java.util.Scanner;

/**
 * Created by User on 03.04.2016.
 */
public class Run {
    private static ExhibitDaoImpl exhibitDao = ExhibitDaoImpl.getInstance();
    private static InventoryDaoImpl inventoryDao = InventoryDaoImpl.getInstance();

    public static void menu() {
        int action = 0;
        int exhibitToDelete = 0;
        boolean exit = false;
        Scanner inputReader = new Scanner(System.in);
        while (!exit) {
            System.out.println("\nMenu:");
            System.out.println("1 - вывести все экспонаты");
            System.out.println("2 - вывести все инвенторные записи");
            System.out.println("3 - провести инвентаризацию");
            System.out.println("4 - добавить эксспонат");
            System.out.println("5 - удалить экспонат");
            System.out.println("0 - выйти");
            action = inputReader.nextInt();

            switch (action) {
                case 1:
                    MuseumWriter.writeExhibits(exhibitDao.getAll());
                    break;
                case 2:
                    MuseumWriter.writeInventory(inventoryDao.getAll());
                    break;
                case 3:
                    System.out.println("После очередной бурной нои в музее .... ");
                    Inventorisation.makeInventorisation();
                    MuseumWriter.writeInventory(inventoryDao.getAll());
                    break;
                case 4:
                    Exhibit exhibit = new Exhibit();
                    System.out.println("введите название экспоната");
                    exhibit.setName(inputReader.next());
                    System.out.println("введите описание экспоната");
                    exhibit.setDescription(inputReader.next());

                    try {
                        exhibitDao.add(exhibit);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    System.out.println("добавлено!..");
                    break;
                case 5:
                    System.out.println("введите айди удаляемого экспоната");
                    MuseumWriter.writeExhibits(exhibitDao.getAll());
                    exhibitToDelete = inputReader.nextInt();
                    try {
                        exhibitDao.delete(exhibitDao.get(exhibitToDelete));
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    exhibitToDelete = 0;
                    System.out.println("удалено!..");
                    break;
                case 0:
                    exit = true;
                    break;
                default:
                    break;
            }
            if (exit) {
                break;
            }
        }
    }

    public static void main(String[] args) {

        menu();
    }
}

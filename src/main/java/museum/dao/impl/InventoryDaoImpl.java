package museum.dao.impl;

import museum.dao.MuseumDao;
import museum.model.Inventory;
import museum.util.HibernateUtil;
import org.hibernate.Session;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by User on 03.04.2016.
 */
public class InventoryDaoImpl implements MuseumDao<Inventory> {

    private final static InventoryDaoImpl instance;

    static {
        instance = new InventoryDaoImpl();
    }

    private InventoryDaoImpl() {
    }

    public static InventoryDaoImpl getInstance() {
        return instance;
    }

    /**
     * The method used for adding some record in database table.
     *
     * @param inventory the object which will be added in database
     */
    public void add(Inventory inventory) throws SQLException {
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(inventory);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen())) {
                session.close();
            }
        }
    }

    /**
     * The method used for deleting some record from database table.
     *
     * @param inventory the object which will be deleted in database
     */
    public void delete(Inventory inventory) throws SQLException {
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();

            session.beginTransaction();
            session.delete(inventory);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen())) {
                session.close();
            }
        }
    }

    /**
     * 111The method used for getting some record from database table.
     *
     * @param key the primary key of record which will be gotten
     * @return T object, where T is the type of object stored as record in database.
     */
    public Inventory get(int key) throws SQLException {
        Inventory inventory = null;
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            inventory = session.get(Inventory.class, key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen())) {
                session.close();
            }
        }
        return inventory;
    }

    /**
     * The method used for getting all records from database table. Has no input parameters
     *
     * @return List of T objects, where T is the type of objects stored as records in database.
     */
    public List<Inventory> getAll() {
        List<Inventory> inventories = null;
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            inventories = session.createCriteria(Inventory.class).list();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen())) {
                session.close();
            }
        }
        Set<Inventory> setInv = new HashSet<Inventory>(inventories);
        inventories.clear();
        inventories.addAll(setInv);
        return inventories;
    }

    /**
     * The method used for update some record in database table.
     *
     * @param inventory the object which will be updated in database
     */
    public void saveOrUpdate(Inventory inventory) throws SQLException {
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();

            session.beginTransaction();
            session.saveOrUpdate(inventory);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen())) {
                session.close();
            }
        }
    }
}

package museum.dao.impl;

import museum.dao.MuseumDao;
import museum.model.Exhibit;
import museum.util.HibernateUtil;
import org.hibernate.Session;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by User on 03.04.2016.
 */
public class ExhibitDaoImpl implements MuseumDao<Exhibit> {
    private final static ExhibitDaoImpl instance;

    static {
        instance = new ExhibitDaoImpl();
    }

    private ExhibitDaoImpl() {
    }

    public static ExhibitDaoImpl getInstance() {
        return instance;
    }

    /**
     * The method used for adding some record in database table.
     *
     * @param exhibit the object which will be added in database
     */
    public void add(Exhibit exhibit) throws SQLException {
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(exhibit);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen())) {
                session.close();
            }
        }
    }


    /**
     * The method used for deleting some record from database table.
     *
     * @param exhibit the object which will be deleted in database
     */
    public void delete(Exhibit exhibit) throws SQLException {
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();

            session.beginTransaction();
            session.delete(exhibit);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen())) {
                session.close();
            }
        }
    }

    /**
     * 111The method used for getting some record from database table.
     *
     * @param key the primary key of record which will be gotten
     * @return T object, where T is the type of object stored as record in database.
     */
    public Exhibit get(int key) throws SQLException {
        Exhibit exhibit = null;
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            exhibit = session.get(Exhibit.class, key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen())) {
                session.close();
            }
        }
        return exhibit;
    }

    /**
     * The method used for getting all records from database table. Has no input parameters
     *
     * @return List of T objects, where T is the type of objects stored as records in database.
     */
    public List<Exhibit> getAll() {
        List<Exhibit> exhibits = null;
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            exhibits = session.createCriteria(Exhibit.class).list();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen())) {
                session.close();
            }
        }
        return exhibits;
    }

    /**
     * The method used for update some record in database table.
     *
     * @param exhibit the object which will be updated in database
     */
    public void saveOrUpdate(Exhibit exhibit) throws SQLException {
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();

            session.beginTransaction();
            session.saveOrUpdate(exhibit);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen())) {
                session.close();
            }
        }
    }
}

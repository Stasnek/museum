package museum.controller.util;

import museum.dao.impl.ExhibitDaoImpl;
import museum.model.Exhibit;

import java.sql.SQLException;
import java.util.Random;

/**
 * Created by User on 05.04.2016.
 */
public class MuseumNight {
    public static void makeParty() {
        Random random = new Random();
        ExhibitDaoImpl exhibitDao = ExhibitDaoImpl.getInstance();
        for (Exhibit exhibit : exhibitDao.getAll()) {
            exhibit.setPresent(random.nextBoolean());
            try {
                exhibitDao.saveOrUpdate(exhibit);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}

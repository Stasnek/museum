package museum.controller;

import museum.model.Exhibit;
import museum.model.Inventory;

import java.util.List;

/**
 * Created by User on 05.04.2016.
 */
public class MuseumWriter {

    public static void writeExhibits(List<Exhibit> exhibits) {
        String isPresent = "выбежал ночью";
        for (Exhibit exhibit : exhibits) {
            if (exhibit.isPresent()) {
                isPresent = "на месте";
            }
            System.out.println(exhibit.getId() + "    " + exhibit.getName() + "   " + isPresent + "         " + exhibit.getDescription());
        }
    }

    public static void writeExhibits(Exhibit exhibit) {
        String isPresent = "выбежал ночью";
        if (exhibit.isPresent()) {
            isPresent = "на месте";
        }
        System.out.println(exhibit.getId() + "    " + exhibit.getName() + "   " + isPresent + "     " + exhibit.getDescription());
    }

    public static void writeInventory(List<Inventory> inventories) {
        for (Inventory inventory : inventories) {
            System.out.println();
            System.out.println(inventory.getId() + "  всего экспонатов: " + inventory.getTotalExhibits() + "  " +
                    "  экспонатов убежало: " + (inventory.getTotalExhibits() - inventory.getAttendeesExhibits()));
            System.out.println("список экспонатов:");
            writeExhibits(inventory.getExhibits());
        }
    }

    public static void writeInventory(Inventory inventory) {
        System.out.println();
        System.out.println(inventory.getId() + "  всего экспонатов: " + inventory.getTotalExhibits() + "  " +
                "  экспонатов убежало: " + (inventory.getTotalExhibits() - inventory.getAttendeesExhibits()));
        System.out.println("список экспонатов:");
        writeExhibits(inventory.getExhibits());

    }
}

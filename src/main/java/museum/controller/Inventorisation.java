package museum.controller;

import museum.controller.util.MuseumNight;
import museum.dao.impl.ExhibitDaoImpl;
import museum.dao.impl.InventoryDaoImpl;
import museum.model.Exhibit;
import museum.model.Inventory;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by User on 05.04.2016.
 */
public class Inventorisation {
    private static int calculateAttendeesExhibits(List<Exhibit> exhibits) {
        int count = 0;
        for (Exhibit exhibit : exhibits) {
            if (!exhibit.isPresent()) {
                count++;
            }
        }
        return count;
    }

    public static void makeInventorisation() {
        InventoryDaoImpl inventoryDao = InventoryDaoImpl.getInstance();
        ExhibitDaoImpl exhibitDao = ExhibitDaoImpl.getInstance();
        MuseumNight.makeParty();
        Inventory inventory = new Inventory();
        List<Exhibit> exhibits = exhibitDao.getAll();
        inventory.setExhibits(exhibits);
        inventory.setTotalExhibits(exhibits.size());
        inventory.setAttendeesExhibits(calculateAttendeesExhibits(exhibits));
        try {
            inventoryDao.add(inventory);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

package museum.util;

import museum.model.Exhibit;
import museum.model.Inventory;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

    /**
     * A variable representing instance of SessionFactory class.
     */
    private static SessionFactory sessionFactory;

    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure("hibernate.cfg.xml");
            configuration.addAnnotatedClass(Exhibit.class);
            configuration.addAnnotatedClass(Inventory.class);
            StandardServiceRegistryBuilder standardServiceRegistryBuilder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
            sessionFactory = configuration.buildSessionFactory(standardServiceRegistryBuilder.build());
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    /**
     * The method used for getting sessionFactory value.
     */
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}

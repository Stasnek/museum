package museum.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by User on 02.04.2016.
 */
@Entity
@Table(name = "Inventory_Record")
public class Inventory {
    @Id
    @Column(name = "inventory_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private int totalExhibits;
    private int attendeesExhibits;
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "Exhibit_Inventory", joinColumns = @JoinColumn(name = "inventory_id"), inverseJoinColumns = @JoinColumn(name = "exhibit_id"))
    private List<Exhibit> exhibits;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTotalExhibits() {
        return totalExhibits;
    }

    public void setTotalExhibits(int totalExhibits) {
        this.totalExhibits = totalExhibits;
    }

    public int getAttendeesExhibits() {
        return attendeesExhibits;
    }

    public void setAttendeesExhibits(int attendeesExhibits) {
        this.attendeesExhibits = attendeesExhibits;
    }

    public List<Exhibit> getExhibits() {
        return exhibits;
    }

    public void setExhibits(List<Exhibit> exhibits) {
        this.exhibits = exhibits;
    }
}
